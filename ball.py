#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sprite import *
from const import *


class Ball(Sprite):

    def __init__(self, name, surface, mask=None):
        Sprite.__init__(self, name, surface, mask)

        self.active = False
        self.last_touch = 0
        self.touches_cnt = 0

    def update(self):
        if self.active:
            self.vel[1] += B_GRAVITY

        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]

    def is_outside_fence(self, left, top, right, bottom):
        #return self.pos[0] + self.size[0] < left.pos[0] or self.pos[0] > right.pos[0] + right.size[0] or \
        #       self.pos[1] + self.size[1] < top.pos[1] or self.pos[1] > bottom.pos[1] + bottom.size[1]
        # TODO
        return False

    def field_side(self, bg):
        return 1 if self.pos[0] > bg[0] / 2 else 0
