#!/usr/bin/env python

import pygame


def mask_from_surface(surface, threshold=127):
    #return pygame.mask.from_surface(surface, threshold)

    mask = pygame.mask.Mask(surface.get_size())
    key = surface.get_colorkey()
    if key:
        for y in range(surface.get_height()):
            for x in range(surface.get_width()):
                if surface.get_at((x+0.1, y+0.1)) != key:
                    mask.set_at((x, y), 1)
    else:
        for y in range(surface.get_height()):
            for x in range(surface.get_width()):
                if surface.get_at((x, y))[3] > threshold:
                    mask.set_at((x, y), 1)
    return mask


def vadd(x, y):
    return [x[0]+y[0], x[1]+y[1]]


def vsub(x, y):
    return [x[0]-y[0], x[1]-y[1]]


def vdot(x, y):
    return x[0]*y[0]+x[1]*y[1]


class Sprite:
    def __init__(self, name, surface, mask=None):
        self.surface = surface
        if mask:
            self.mask = mask
        else:
            self.mask = mask_from_surface(self.surface)
        self.pos = [0, 0]
        self.vel = [0, 0]
        self.size = [0, 0]
        self.name = name

    def set_size(self, size):
        self.size = size

    def setPos(self, pos):
        self.pos = [pos[0], pos[1]]

    def setVelocity(self, vel):
        self.vel = [vel[0], vel[1]]

    def kick(self, impulse):
        self.vel[0] += impulse[0]
        self.vel[1] += impulse[1]

    def collide(self, s):
        """Test if the sprites are colliding and
        resolve the collision in this case."""
        offset = [int(x) for x in vsub(s.pos, self.pos)]
        overlap = self.mask.overlap_area(s.mask, offset)
        if overlap == 0:
            return
        """Calculate collision normal"""
        nx = (self.mask.overlap_area(s.mask, (offset[0]+1, offset[1])) -
              self.mask.overlap_area(s.mask, (offset[0]-1, offset[1])))
        ny = (self.mask.overlap_area(s.mask, (offset[0], offset[1]+1)) -
              self.mask.overlap_area(s.mask, (offset[0], offset[1]-1)))
        if nx == 0 and ny == 0:
            """One sprite is inside another"""
            return
        n = [nx, ny]
        dv = vsub(s.vel, self.vel)
        J = vdot(dv, n)/(2*vdot(n, n))
        if J > 0:
            """Can scale up to 2*J here to get bouncy collisions"""
            J *= 1.9
            self.kick([nx*J,  ny*J])
            s.kick([-J*nx, -J*ny])

    def update(self):
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]
