#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sprite import *
from const import *
import math


class Player(Sprite):

    def __init__(self, name, surface, mask=None):
        Sprite.__init__(self, name, surface, mask)

        self.onGround = True
        self.up_pressed = 0

    def update_p(self, left, right, up):
        if up:
            #print self.up_pressed, self.vel[1]
            if self.onGround:  # прыгаем, только когда можем оттолкнуться от земли
                self.onGround = False
            if self.up_pressed < UP_LIM:
                self.vel[1] = -YVEL_MIN - float(YVEL_MAX - YVEL_MIN) / UP_LIM * self.up_pressed
            self.up_pressed += 1
        else:
            self.up_pressed = 0
        if left:
            self.vel[0] = -MOVE_SPEED  # Лево = x- n
        if right:
            self.vel[0] = MOVE_SPEED  # Право = x + n
        if not(left or right):  # стоим, когда нет указаний идти
            self.vel[0] = 0

        if not self.onGround:
            self.vel[1] += GRAVITY

        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]

    def collide(self, s):
        """Test if the sprites are colliding and
        resolve the collision in this case."""
        offset = [int(x) for x in vsub(s.pos, self.pos)]
        overlap = self.mask.overlap_area(s.mask, offset)
        if overlap == 0:
            return

        if s.name == 'ball':
            s.active = True
            if s.last_touch == 0 and self.name == 'player1':
                s.touches_cnt += 1
            elif s.last_touch == 1 and self.name == 'player2':
                s.touches_cnt += 1
            elif s.last_touch == 0 and self.name == 'player2':
                s.last_touch = 1
                s.touches_cnt = 1
            elif s.last_touch == 1 and self.name == 'player1':
                s.last_touch = 0
                s.touches_cnt = 1
            print s.last_touch, s.touches_cnt

        """Calculate collision normal"""
        nx = (self.mask.overlap_area(s.mask, (offset[0]+1, offset[1])) -
              self.mask.overlap_area(s.mask, (offset[0]-1, offset[1])))
        ny = (self.mask.overlap_area(s.mask, (offset[0], offset[1]+1)) -
              self.mask.overlap_area(s.mask, (offset[0], offset[1]-1)))

        if nx == 0 and ny == 0:
            """One sprite is inside another"""
            print "One sprite is inside another"
            return

        n_mod = math.sqrt(nx**2 + ny**2)
        nx /= n_mod
        ny /= n_mod
        n, np = [nx, ny], [ny, -nx]
        dv = vsub(s.vel, self.vel)
        dv_n = vdot(dv, n) * 1.
        dv_np = vdot(dv, np)

        if dv_n > 0:
            s.vel = vadd([np[0]*dv_np, np[1]*dv_np], [-n[0]*dv_n, -n[1]*dv_n])
            s.vel = vadd(s.vel, self.vel)

    def collide_stop(self, left, bottom, right):
        if self.pos[1]+self.size[1] > bottom.pos[1]:  # bottom
            #print 'bottom'
            self.pos[1] = bottom.pos[1] - self.size[1]
            self.vel[1] = 0
            self.onGround = True
        elif self.pos[0]+self.size[0] > right.pos[0]:  # right
            #print 'right'
            self.pos[0] = right.pos[0] - self.size[0]
            self.vel[0] = 0
        elif self.pos[0] < left.pos[0] + left.size[0]:  # left
            #print 'left'
            self.pos[0] = left.pos[0] + left.size[0]
            self.vel[0] = 0
