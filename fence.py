#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sprite import *
import math


class Fence(Sprite):
    def collide(self, s):
        is_ball_down = False

        """Test if the sprites are colliding and
        resolve the collision in this case."""
        offset = [int(x) for x in vsub(s.pos, self.pos)]
        overlap = self.mask.overlap_area(s.mask, offset)
        if overlap == 0:
            return is_ball_down

        if self.name == 'bottom' and s.name == 'ball':
            is_ball_down = True

        """Calculate collision normal"""
        nx = (self.mask.overlap_area(s.mask, (offset[0]+1, offset[1])) -
              self.mask.overlap_area(s.mask, (offset[0]-1, offset[1])))
        ny = (self.mask.overlap_area(s.mask, (offset[0], offset[1]+1)) -
              self.mask.overlap_area(s.mask, (offset[0], offset[1]-1)))
        if nx == 0 and ny == 0:
            """One sprite is inside another"""
            if self.name == 'bottom': nx, ny = 0, 1
            elif self.name == 'top': nx, ny = 0, -1
            elif self.name == 'right': nx, ny = 1, 0
            elif self.name == 'left': nx, ny = -1, 0
            else: return is_ball_down

        n_mod = math.sqrt(nx**2 + ny**2)
        nx /= n_mod
        ny /= n_mod
        n, np = [nx, ny], [ny, -nx]
        dv = s.vel
        dv_n = vdot(dv, n) * 1.
        dv_np = vdot(dv, np)

        s.vel = vadd([np[0]*dv_np, np[1]*dv_np], [-n[0]*dv_n, -n[1]*dv_n])

        return is_ball_down

    def collide_stop(self, s):
        offset = [int(x) for x in vsub(s.pos, self.pos)]
        overlap = self.mask.overlap_area(s.mask, offset)
        if overlap == 0:
            return

        nx = (self.mask.overlap_area(s.mask, (offset[0]+1, offset[1])) -
              self.mask.overlap_area(s.mask, (offset[0]-1, offset[1])))
        ny = (self.mask.overlap_area(s.mask, (offset[0], offset[1]+1)) -
              self.mask.overlap_area(s.mask, (offset[0], offset[1]-1)))

        if ny > 0:  # bottom
            print 'bottom'
            s.pos[1] = self.pos[1] - self.size[1] - s.size[1]
            s.vel[1] = 0
            s.onGround = True
        elif ny < 0:  # top
            print 'top'
            s.pos[1] = self.pos[1] + self.size[1]
            s.vel[1] = 0
        elif nx > 0:  # right
            print 'right'
            s.pos[0] = self.pos[0] - s.size[0]
            s.vel[0] = 0
        elif nx < 0:  # left
            print 'left'
            s.pos[0] = self.pos[0] + self.size[0]
            s.vel[0] = 0
