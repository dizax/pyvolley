#!/usr/bin/env python
# -*- coding: utf-8 -*-

MOVE_SPEED = 8
UP_LIM = 15
YVEL_MIN, YVEL_MAX = 8., 12.  # 3 7
GRAVITY = 1.  # 0.5
B_GRAVITY = 0.1
