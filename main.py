#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from pygame import *
from sprite import *
from player import Player
from fence import Fence
from ball import Ball

W_SIZE = (800, 600)
F_PATHS = ['left.bmp', 'top.bmp', 'right.bmp', 'bottom.bmp', 'fence.bmp']
NAMES = ['left', 'top', 'right', 'bottom', 'fence']
F_POS = [(0, 0), (0, 0), (800-7, 0), (0, 600-5), (400-7, 600-325)]

player_path = 'player.png'
ball_path = 'ball.png'

p1_pos, p2_pos = (200-32, 600-10-90), (600-32, 600-10-90)
ball_pos = [(200-32, 500-10-90-120), (600-32, 500-10-90-120)]

score1_pos, score2_pos = (20, 20), (800-40, 20)

podacha = 0
touches_cnt = 0
score = [0, 0]


def reset(player1, player2, ball, m_p):
    #player1.setPos(p1_pos)
    #player1.setVelocity((0, 0))
    #player2.setPos(p2_pos)
    #player2.setVelocity((0, 0))
    ball.setPos([ball_pos[m_p][0]+random.randint(-100, 100), ball_pos[m_p][1]+random.randint(-100, 100)])
    ball.setVelocity((0, 0))
    ball.active = False
    ball.last_touch = podacha
    ball.touches_cnt = 0


def main():
    left1 = right1 = left2 = right2 = False
    up1 = up2 = False

    pygame.init()

    bg = pygame.image.load('field.bmp')
    W_SIZE = bg.get_rect().size
    screen = pygame.display.set_mode(W_SIZE)

    # scores text
    myfont = pygame.font.SysFont("monospace", 30)

    # init field with bounds
    f_sprites = []
    for impath, impos, name in zip(F_PATHS, F_POS, NAMES):
        f_image = pygame.image.load(impath).convert_alpha()
        f_mask = mask_from_surface(f_image)
        s = Fence(name, f_image, f_mask)
        s.set_size(f_image.get_rect().size)
        s.setPos(impos)
        s.setVelocity((0, 0))
        f_sprites.append(s)

    # init players
    p_image = pygame.image.load(player_path).convert_alpha()
    p_mask = mask_from_surface(p_image)
    player1 = Player('player1', p_image, p_mask)
    player1.set_size(p_image.get_rect().size)
    player1.setPos(p1_pos)
    player1.setVelocity((0, 0))
    player2 = Player('player2', p_image, p_mask)
    player2.set_size(p_image.get_rect().size)
    player2.setPos(p2_pos)
    player2.setVelocity((0, 0))

    # init ball
    b_image = pygame.image.load(ball_path).convert_alpha()
    b_mask = mask_from_surface(b_image)
    ball = Ball('ball', b_image, b_mask)
    ball.setPos(ball_pos[0])
    ball.setVelocity((0, 0))

    timer = pygame.time.Clock()

    while 1:  # Основной цикл программы
        timer.tick(60)
        for e in pygame.event.get():  # Обрабатываем события
            if e.type == QUIT:
                raise SystemExit, "QUIT"
            if e.type == KEYDOWN and e.key == K_UP:
                up2 = True
            if e.type == KEYDOWN and e.key == K_LEFT:
                left2 = True
            if e.type == KEYDOWN and e.key == K_RIGHT:
                right2 = True

            if e.type == KEYUP and e.key == K_UP:
                up2 = False
            if e.type == KEYUP and e.key == K_RIGHT:
                right2 = False
            if e.type == KEYUP and e.key == K_LEFT:
                left2 = False

            if e.type == KEYDOWN and e.key == K_w:
                up1 = True
            if e.type == KEYDOWN and e.key == K_a:
                left1 = True
            if e.type == KEYDOWN and e.key == K_d:
                right1 = True

            if e.type == KEYUP and e.key == K_w:
                up1 = False
            if e.type == KEYUP and e.key == K_d:
                right1 = False
            if e.type == KEYUP and e.key == K_a:
                left1 = False

        # players update
        player1.update_p(left1, right1, up1)
        player2.update_p(left2, right2, up2)

        # ball update
        ball.update()

        # collisions
        is_ball_down = False
        player1.collide_stop(f_sprites[0], f_sprites[3], f_sprites[4])
        player2.collide_stop(f_sprites[4], f_sprites[3], f_sprites[2])
        for f in f_sprites:
            is_ball_down = is_ball_down or f.collide(ball)
        player1.collide(ball)
        player2.collide(ball)

        # draw
        screen.blit(bg, (0, 0))
        for s in f_sprites:
            screen.blit(s.surface, s.pos)
        screen.blit(player1.surface, player1.pos)
        screen.blit(player2.surface, player2.pos)
        screen.blit(ball.surface, ball.pos)
        label1 = myfont.render(str(score[0]), 1, (255, 255, 0))
        label2 = myfont.render(str(score[1]), 1, (255, 255, 0))
        screen.blit(label1, score1_pos)
        screen.blit(label2, score2_pos)

        #ar = pygame.PixelArray(screen)
        #ar[:, ::20] = (0, 0, 255)
        #del ar

        pygame.display.update()

        if ball.is_outside_fence(f_sprites[0], f_sprites[1], f_sprites[2], f_sprites[3]):
            podacha = int(not bool(ball.last_touch))
            score[podacha] += 1
            reset(player1, player2, ball, podacha)
        elif ball.touches_cnt > 10:
            podacha = int(not bool(ball.last_touch))
            score[podacha] += 1
            reset(player1, player2, ball, podacha)
        elif is_ball_down:
            field_side_down = ball.field_side(W_SIZE)
            podacha = int(not bool(field_side_down))
            score[podacha] += 1
            reset(player1, player2, ball, podacha)

if __name__ == '__main__':
    main()

