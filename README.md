####Description:
Volley game written with pygame.

####Requirements:
* python;  
* pygame.

####Usage:
Run main.py.  
For movement use arrow keys for player1 and WASD for player2.

